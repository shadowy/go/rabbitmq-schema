<a name="unreleased"></a>
## [Unreleased]


<a name="v1.0.4"></a>
## [v1.0.4] - 2020-02-03
### Features
- change exchange build flow


<a name="v1.0.3"></a>
## [v1.0.3] - 2019-10-21

<a name="v1.0.2"></a>
## [v1.0.2] - 2019-10-21

<a name="v1.0.1"></a>
## v1.0.1 - 2019-10-21
### Code Refactoring
- add dist to .gitignore doc: fix issue in README.md


[Unreleased]: https://gitlab.com/shadowy/go/rabbitmq-schema/compare/v1.0.4...HEAD
[v1.0.4]: https://gitlab.com/shadowy/go/rabbitmq-schema/compare/v1.0.3...v1.0.4
[v1.0.3]: https://gitlab.com/shadowy/go/rabbitmq-schema/compare/v1.0.2...v1.0.3
[v1.0.2]: https://gitlab.com/shadowy/go/rabbitmq-schema/compare/v1.0.1...v1.0.2
