FROM shadowy/build-golang:v0.9.0 as builder
ENV GO111MODULE=on
ENV CGO_ENABLED=0
ENV GOOS=linux
WORKDIR /go/src/gitlab.com/shadowy/go/rabbitmq-schema
COPY . .
RUN make build

FROM alpine:3.20
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/shadowy/go/rabbitmq-schema/dist/rabbitmq-schema .
CMD ["/bin/sh", "-c", "/app/rabbitmq-schema -url=${CONNECTION_STRING}"]
