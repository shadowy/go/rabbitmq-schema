version :=`git describe --abbrev=0 --tags $(git rev-list --tags --max-count=1)`
dockerfile = $(subst .docker,,$@)
CI_REGISTRY_IMAGE?=registry.gitlab.com/shadowy/go/rabbitmq-schema

all: test lint-full

test:
	@echo ">> test"

lint-full: lint card

lint-badge-full: lint card card-badge

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run

card-badge:
	@echo ">>card-badge"
	@curl -X GET 'https://goreportcard.com/checks?repo=gitlab.com/shadowy/go/application-settings'

build:
	@echo ">>build"
	@go build -o ./dist/rabbitmq-schema

docker:
	@echo ">>docker"
	docker build --file ./docker/Dockerfile . -t test-nginx

docker-release: build docker-image

docker-login:
	@echo ">>docker-login"
	@docker login -u gitlab-ci-token -p $(CI_BUILD_TOKEN) registry.gitlab.com

docker-image:
	@echo ">>docker-image"
	docker build --file Dockerfile . -t ${CI_REGISTRY_IMAGE}/cmd:$(version)
	docker push ${CI_REGISTRY_IMAGE}/cmd:$(version)

release: docker-login docker-image
