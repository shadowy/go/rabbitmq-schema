# RabbitMQ Schema Creator
<span style="color:red">*Not tested*</span>

RabbitMQ Schema - tools to create queues and exchange in RabbitMQ based on the prepared schema.

## Content
* [Install](#install)
* [Usage](#usage)
* [Schema](#schema)
* [Plan](#plan)
* [Changelog](#changelog)
* [License](#license)

## Install

````bash
go get -u gitlab.com/shadowy-go/rabbitmq-schema
````
This will install the `rabbitmq-schema` binary to your `$GOPATH/bin` directory.

## Usage

````bash
rabbitmq-schema -url <connection string to rabbit-mq> -schema <schema file>
````

| Parameter | Description |
| --- |:--|
| `url` | connection string to rabbit-mq (example: 'amqp://guest:guest@localhost:5672/') |
| `schema` | schema file |

## Schema

Example of schema file:
````yaml
schema:
  exchange:
    example-fanout:
      type: fanout
      durable: true
      autoDelete: false
    example-topic:
      type: topic
      durable: true
      autoDelete: false
      bind:
        - exchange: example-fanout
  queue:
    example-queue:
      durable: true
      autoDelete: false
      maxPriority: 1
      bind:
        - exchange: example-topic
          key: "*.*.ticker"
        - exchange: example-topic
          key: "*.*.ticker1"
````

## Plan


## Changelog

## License

Licensed under [MIT License](./LICENSE)
