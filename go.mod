module gitlab.com/shadowy-go/rabbitmq-schema

go 1.12

require (
	github.com/alecthomas/gometalinter v3.0.0+incompatible
	github.com/fatih/color v1.7.0
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/rabbitmq/amqp091-go v1.10.0
	golang.org/x/sys v0.21.0 // indirect
)
