package metainformation

import (
	"github.com/fatih/color"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/shadowy-go/rabbitmq-schema/util"
)

const (
	ModeQueue    int = 1
	ModeExchange int = 2
)

type Bind struct {
	Exchange string  `yaml:"exchange"`
	Key      *string `yaml:"key"`
	NoWait   *bool   `yaml:"noWait"`
	Headers  *Header `yaml:"header"`
}

func (bind *Bind) init() {
	if bind.Key == nil {
		bind.Key = util.PtrString("")
	}
	if bind.NoWait == nil {
		bind.NoWait = util.PtrBool(false)
	}
	if bind.Headers != nil {
		bind.Headers.init()
	}
}

func (bind Bind) build(name string, mode int, channel *amqp.Channel) {
	isExecuted := false
	var err error
	var args amqp.Table
	headers := ""
	if bind.Headers != nil {
		args, headers = bind.Headers.getArgs()
	}
	if mode == ModeQueue {
		err = channel.QueueBind(name, *bind.Key, bind.Exchange, *bind.NoWait, args)
		isExecuted = true
	} else if mode == ModeExchange {
		err = channel.ExchangeBind(name, *bind.Key, bind.Exchange, *bind.NoWait, args)
		isExecuted = true
	}
	if !isExecuted {
		color.Red("ERROR %s: mode %s not found", name, mode)
		return
	}
	key := ""
	if bind.Key != nil {
		key = *bind.Key
	}
	if err == nil {
		color.Green("BIND %s to %s (%s:%s) - OK", name, bind.Exchange, key, headers)
	} else {
		color.Red("BIND ERROR %s to %s (%s:%s) - %s", name, bind.Exchange, key, headers, err.Error())
	}
}
