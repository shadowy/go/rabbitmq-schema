package metainformation

import (
	"github.com/fatih/color"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/shadowy-go/rabbitmq-schema/util"
)

type Exchange struct {
	Name         string
	Type         *string  `yaml:"type"`
	IsDurable    *bool    `yaml:"durable"`
	IsAutoDelete *bool    `yaml:"autoDelete"`
	IsInternal   *bool    `yaml:"internal"`
	NoWait       *bool    `yaml:"noWait"`
	Bind         *[]*Bind `yaml:"bind"`
}

func (exchange *Exchange) init() {
	if exchange.Type == nil {
		exchange.Type = util.PtrString("topic")
	}
	if exchange.IsDurable == nil {
		exchange.IsDurable = util.PtrBool(true)
	}
	if exchange.IsAutoDelete == nil {
		exchange.IsAutoDelete = util.PtrBool(false)
	}
	if exchange.IsInternal == nil {
		exchange.IsInternal = util.PtrBool(false)
	}
	if exchange.NoWait == nil {
		exchange.NoWait = util.PtrBool(false)
	}
	if exchange.Bind != nil {
		for _, bind := range *exchange.Bind {
			bind.init()
		}
	}
}

func (exchange Exchange) buildExchange(channel *amqp.Channel) {
	err := channel.ExchangeDeclare(exchange.Name, *exchange.Type, *exchange.IsDurable, *exchange.IsAutoDelete,
		*exchange.IsInternal, *exchange.NoWait, nil)
	if err != nil {
		color.Red("EXCHANGE: %s - ERROR: %s", exchange.Name, err.Error())
	} else {
		color.Green("EXCHANGE: %s - OK", exchange.Name)
	}
}

func (exchange Exchange) buildBind(channel *amqp.Channel) {
	if exchange.Bind != nil {
		color.Blue("EXCHANGE BIND: %s", exchange.Name)
		for _, bind := range *exchange.Bind {
			bind.build(exchange.Name, ModeExchange, channel)
		}
	}
}
