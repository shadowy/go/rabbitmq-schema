package metainformation

import (
	"github.com/alecthomas/gometalinter/_linters/src/gopkg.in/yaml.v2"
	"github.com/fatih/color"
	"io/ioutil"
)

type File struct {
	Schema Schema `yaml:"schema"`
}

func (file File) FromFile(filename string) (result *File, err error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		color.Red("Error:\t%s", err.Error())
		return
	}
	var d File
	err = yaml.Unmarshal(data, &d)
	if err != nil {
		color.Red("Error:\t%s", err.Error())
		return
	}
	d.Schema.init()
	result = &d
	return
}
