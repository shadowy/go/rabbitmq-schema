package metainformation

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/shadowy-go/rabbitmq-schema/util"
)

type Header struct {
	Match *string            `yaml:"match"`
	Keys  *map[string]string `yaml:"keys"`
}

func (header *Header) init() {
	if header.Match == nil {
		header.Match = util.PtrString("all")
	}
	if header.Keys == nil {
		mapData := make(map[string]string)
		header.Keys = &mapData
	}
}

func (header Header) getArgs() (result amqp.Table, headers string) {
	result = amqp.Table{}
	result["x-match"] = *header.Match
	headers = "x-match=" + *header.Match + ";"
	for key, value := range *header.Keys {
		result[key] = value
		headers = headers + key + "=" + value + ";"
	}
	return
}
