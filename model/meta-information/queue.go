package metainformation

import (
	"github.com/fatih/color"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/shadowy-go/rabbitmq-schema/util"
)

type Queue struct {
	Name         string
	IsDurable    *bool    `yaml:"durable"`
	IsAutoDelete *bool    `yaml:"autoDelete"`
	IsExclusive  *bool    `yaml:"exclusive"`
	NoWait       *bool    `yaml:"noWait"`
	MaxPriority  *int     `yaml:"maxPriority"`
	Bind         *[]*Bind `yaml:"bind"`

	attr amqp.Table
}

func (queue *Queue) init() {
	if queue.IsDurable == nil {
		queue.IsDurable = util.PtrBool(true)
	}
	if queue.IsAutoDelete == nil {
		queue.IsAutoDelete = util.PtrBool(false)
	}
	if queue.IsExclusive == nil {
		queue.IsExclusive = util.PtrBool(false)
	}
	if queue.NoWait == nil {
		queue.NoWait = util.PtrBool(false)
	}
	if queue.Bind != nil {
		for _, bind := range *queue.Bind {
			bind.init()
		}
	}
	queue.attr = make(amqp.Table)
	if queue.MaxPriority != nil {
		queue.attr["x-max-priority"] = *queue.MaxPriority
	}
}

func (queue Queue) build(channel *amqp.Channel) {
	_, err := channel.QueueDeclare(queue.Name, *queue.IsDurable, *queue.IsAutoDelete, *queue.IsExclusive,
		*queue.NoWait, queue.attr)
	if err != nil {
		color.Red("QUEUE: %s - ERROR: %s", queue.Name, err.Error())
	} else {
		color.Green("QUEUE: %s - OK", queue.Name)
	}
	if queue.Bind != nil {
		color.Blue("QUEUE BIND: %s", queue.Name)
		for _, bind := range *queue.Bind {
			bind.build(queue.Name, ModeQueue, channel)
		}
	}
}
