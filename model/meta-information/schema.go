package metainformation

import (
	"github.com/fatih/color"
	amqp "github.com/rabbitmq/amqp091-go"
)

type Schema struct {
	Exchange map[string]*Exchange `yaml:"exchange"`
	Queue    map[string]*Queue    `yaml:"queue"`
}

func (schema *Schema) init() {
	for key, value := range schema.Exchange {
		value.Name = key
		value.init()
	}
	for key, value := range schema.Queue {
		value.Name = key
		value.init()
	}
}

func (schema Schema) Build(channel *amqp.Channel) {
	color.Cyan("----------------------------------------------------------")
	color.Cyan("Build Exchange")
	color.Cyan("----------------------------------------------------------")
	for _, value := range schema.Exchange {
		value.buildExchange(channel)
	}
	for _, value := range schema.Exchange {
		value.buildBind(channel)
	}
	color.Cyan("----------------------------------------------------------")
	color.Cyan("Build Queue")
	color.Cyan("----------------------------------------------------------")
	for _, value := range schema.Queue {
		value.build(channel)
	}
}
