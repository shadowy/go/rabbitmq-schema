package main

import (
	"flag"
	"github.com/fatih/color"
	amqp "github.com/rabbitmq/amqp091-go"
	metaInformation "gitlab.com/shadowy-go/rabbitmq-schema/model/meta-information"
	"os"
)

func main() {
	color.Green("----------------------------------------------------------")
	color.Green("rabbitmq-schema ver 0.0.1")
	color.Green("----------------------------------------------------------")
	fileName := flag.String("schema", "rabbitmq-schema.yaml", "schema for rabbitmq")
	connectionString := flag.String("url", "", "connection url to RabbitMq")
	flag.Parse()
	color.Green("schema\t=\t%s\n", *fileName)
	color.Green("url\t=\t%s\n", *connectionString)
	color.Green("----------------------------------------------------------")
	file, err := metaInformation.File{}.FromFile(*fileName)
	if err != nil {
		os.Exit(1)
		return
	}

	connect, err := amqp.Dial(*connectionString)
	if err != nil {
		color.Red("Connect error:\t%s", err.Error())
		os.Exit(2)
		return
	}
	defer func() {
		_ = connect.Close()
	}()

	channel, err := connect.Channel()
	if err != nil {
		color.Red("Channel error:\t%s", err.Error())
		os.Exit(2)
		return
	}
	defer func() {
		_ = channel.Close()
	}()

	file.Schema.Build(channel)
}
